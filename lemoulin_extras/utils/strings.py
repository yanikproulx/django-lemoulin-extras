from django.utils.html import strip_tags


def unescape(string):
	# Python 3.4+
	try:
		import html.unescape
		return html.unescape(string)

	except:
		# Python 3.3-
		try:
			import html.parser
			return html.parser.HTMLParser().unescape(string)

		except:
			return string


def strip_html(string):
	return strip_tags(unescape(string))
