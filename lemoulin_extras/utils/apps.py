from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = 'lemoulin_extras.utils'
