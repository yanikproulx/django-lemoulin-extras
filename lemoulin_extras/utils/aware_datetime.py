"""
Timezone Awarde Datetime functions
"""

from django.utils import timezone
from django.utils.dateparse import parse_datetime


def tz_now():
	"Return aware now()"
	return timezone.localtime(timezone.now())


def tz_today():
	"Return aware today()"
	return tz_now().date()


def tz_aware_datetime(str_datetime, tzinfo=None):
	"Turn string to aware datetime"
	if not tzinfo:
		tzinfo = tz_now().tzinfo

	return parse_datetime(str_datetime).replace(tzinfo=tzinfo)
