from django import template

register = template.Library()


def contains(value, arg):
	"""
	Usage:
	{% if link_url|contains:"youtube" %}
	"""
	return arg in value

register.filter('contains', contains)


def startswith(value, arg):
	"""
	Usage:
	{% if link_url|startswith:"www" %}
	"""
	return value.startswith(arg)

register.filter('startswith', startswith)


def endswith(value, arg):
	"""
	Usage:
	{% if link_url|endswith:".com" %}
	"""
	return value.endswith(arg)

register.filter('endswith', endswith)


@register.filter
def split(value, arg):
	"""
	Usage:
	{% for item in csv_string|split:"," %}
	"""
	return value.split(arg)
