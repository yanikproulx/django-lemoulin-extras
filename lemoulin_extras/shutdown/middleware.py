import re
from django.shortcuts import render_to_response
from django.template import RequestContext

from lemoulin_extras.shutdown import settings


class ShutdownMiddleware(object):
	def __init__(self):
		self.shutdown_site = settings.SHUTDOWN_SITE
		self.template = settings.TEMPLATE
		self.ignored_urls = settings.IGNORED_URLS

	def process_request(self, request):
		# If user is logged in as staff, go right in
		if request.user.is_staff:
			return None

		ignore_urls = tuple([re.compile(r'%s' % str(url)) for url in self.ignored_urls])
		for url in ignore_urls:
			if url.match(request.path_info):
				return None

		if self.shutdown_site:
			return render_to_response(self.template, {}, context_instance=RequestContext(request))
