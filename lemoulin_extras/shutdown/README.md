# Shutdown #

## Installation ##

in settings:

```
INSTALLED_APPS = (
    # ...
    'lemoulin_extras.shutdown',
    'lemoulin_extras',
)
```

```
MIDDLEWARE_CLASSES = (
    # ...
    'lemoulin_extras.shutdown.middleware.ShutdownMiddleware',
)
```

## Usage ##

in `settings.py`:

```
SHUTDOWN_SITE = True
```

You can allow access to some URLs by specifying:

```
SHUTDOWN_IGNORED_URLS = (r'^myapp/$', r'^about/', )
```

default is:

```
(r'^/admin/$', )
```

You can user your own template:

```
lemoulin_extras/shutdown/shutdown.html
```
