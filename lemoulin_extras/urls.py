from django.conf.urls import include, url
# from django.conf.urls.i18n import i18n_patterns


# Locale independent URLs
urlpatterns = [
	url(r'^cache/', include("lemoulin_extras.cache.urls", namespace="cache")),
]


# urlpatterns += i18n_patterns(
# 	url(r'^$', Home.as_view(), name="home"),
# )
