# l10n #

## Installation ##

in settings:

```
INSTALLED_APPS = (
    # ...
    'lemoulin_extras.l10n',
    'lemoulin_extras',
)
```

## Usage ##

in `models.py`:

```
from lemoulin_extras.l10n.models import ModelWithTranslations, MultilingualModel
```

Example of models with translations:

```
class Product(ModelWithTranslations):
    internal_name = models.CharField(verbose_name=_("Internal Name*"), help_text=_("*Only used internally. The translated name will be used on the site."), max_length=256, )
    slug = models.SlugField(verbose_name=_("Slug"), max_length=64, unique=True, )
    image = models.ImageField(verbose_name=_("Image"), upload_to=get_product_files_path, null=True, blank=True, )
    order = models.PositiveIntegerField(verbose_name=_("Display order"), default=99, )

    def __str__(self):
        return self.internal_name

    @property
    def name(self, language_code=None):
        return self.lookup_translation(attr="name", language_code=language_code)

    @property
    def description(self, language_code=None):
        return self.lookup_translation(attr="description", language_code=language_code)

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Product")
```


```
class ProductTranslation(MultilingualModel):
    product = models.ForeignKey(verbose_name=_("Product"), to=Product, related_name="translations", )
    name = models.CharField(verbose_name=_("Name"), max_length=256, )
    description = models.TextField(verbose_name=_("Description"), blank=True, null=True, )

    class Meta:
        verbose_name = _("Translation")
        verbose_name_plural = _("Translations")
        unique_together = ("product", "language_code")
```

in `views.py`:

```
from lemoulin_extras.l10n.views import LanguageMixin
```

example of view returning
```
class AboutPage(LanguageMixin, TemplateView):
    model = models.Page
    context_object_name = "page"
    template_name = "base/about.html"
```
