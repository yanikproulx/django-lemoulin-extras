from django import template
from django.core.urlresolvers import resolve, reverse
from django.utils.translation import activate, get_language

register = template.Library()


@register.filter()
def change_locale(path, language_code):
	"""
	Returns a path in specified language

	ex:
	<a href="{{ request.path|change_locale:language_code }}">{{ language_code }}</a>
	"""
	# Get current language
	cur_language_code = get_language()

	# Resolve path
	resolved_view = resolve(path)

	# Activate specified language
	activate(language_code)

	# Resolve URL in specified language
	url = reverse(viewname=resolved_view.view_name, args=resolved_view.args, kwargs=resolved_view.kwargs)

	# Reactivate current language
	activate(cur_language_code)

	return url
