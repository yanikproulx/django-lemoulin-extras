from django.db import models
from django.utils.translation import ugettext_lazy as _


class DynamicSetting(models.Model):
	key = models.SlugField(verbose_name=_("Key"), max_length=255, unique=True, )
	value = models.TextField(verbose_name=_("Value"), )

	class Meta:
		verbose_name = _("Dynamic Setting")
		verbose_name_plural = _("Dynamic Settings")

	def __str__(self):
		return self.key
