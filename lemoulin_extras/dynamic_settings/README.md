# Extra Context #

## Installation ##

in settings:

```
INSTALLED_APPS = (
	# ...
	'lemoulin_extras.dynamic_settings',
	'lemoulin_extras',
)
```

```
TEMPLATES = [
	{
		# ...
		'OPTIONS': {
			# ...
			'context_processors': [
				# ...
				'lemoulin_extras.dynamic_settings.context_processors.dynamic_settings',
			]
		}
	}
]
```

run `manage.py migrate`

## Usage ##

in `settings.py`:

```
DYNAMIC_SETTINGS = {
	"GOOGLE_ANALYTICS_CODE": "ga_",
}
```

or in DB (will override existing keys from settings)

in `views.py`:

```
from lemoulin_extras.dynamic_settings import dynamic_settings

class ProductDetail(DetailView):
	template_name = "products/detail.html"

	def get_context_data(self, **kwargs):
		context = super(ProductDetail, self).get_context_data(**kwargs)

		DYNAMIC_SETTINGS = dynamic_settings.dynamic_settings
		DYNAMIC_SETTINGS["page_title"] = contexxt["object"].title

		context["DYNAMIC_SETTINGS"] = DYNAMIC_SETTINGS

		return context
```
