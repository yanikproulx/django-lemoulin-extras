from lemoulin_extras.dynamic_settings import dynamic_settings as base_dynamic_settings


def dynamic_settings(request):
	"""
	Simple context processor that puts the DYNAMIC_SETTINGS into every
	RequestContext. Just make sure you have a setting like this:
		TEMPLATES = [
			{
				# ...
				'OPTIONS': {
					# ...
					'context_processors': [
						# ...
						'lemoulin_extras.dynamic_settings.context_processors.dynamic_settings',
					]
				}
			}
		]
	"""

	def get_current_domain(request):
		from django.contrib.sites.shortcuts import get_current_site

		try:
			return get_current_site(request).domain
		except:
			return None

	DYNAMIC_SETTINGS = base_dynamic_settings.dynamic_settings

	DYNAMIC_SETTINGS["CURRENT_DOMAIN"] = get_current_domain(request=request)

	return {
		"DYNAMIC_SETTINGS": DYNAMIC_SETTINGS,
	}
