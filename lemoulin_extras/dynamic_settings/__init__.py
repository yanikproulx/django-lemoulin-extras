from datetime import datetime
from django.conf import settings
from django.utils.functional import LazyObject

default_app_config = 'lemoulin_extras.dynamic_settings.apps.DynamicSettingsConfig'


class DynamicSettings(object):
	def get_hostname(self):
		import socket

		try:
			return socket.gethostname()
		except:
			return 'localhost'

	def get_current_domain(self, request):
		from django.contrib.sites.shortcuts import get_current_site

		try:
			return get_current_site(request).domain
		except:
			return None

	def get_cache_stats(self):
		from django.core.cache import caches

		cache_stats = []

		for cache_backend_nm, cache_backend_attrs in settings.CACHES.items():
			try:
				cache_backend = caches[cache_backend_nm]
				this_backend_stats = cache_backend._cache.get_stats()

				for server_name, server_stats in this_backend_stats:
					cache_stats.append(("%s: %s" % (
						cache_backend_nm, server_name), server_stats))

			except AttributeError:  # this backend probably doesn't support that
				continue

		return cache_stats

	def get_dynamic_settings_from_settings(self):
		"""Get DYNAMIC_SETTINGS from settings.py"""
		try:
			return settings.DYNAMIC_SETTINGS
		except:
			return {}

	def get_dynamic_settings_from_db(self):
		"""Get DYNAMIC_SETTINGS from DB """

		DB_DYNAMIC_SETTINGS = {}

		try:
			from lemoulin_extras.dynamic_settings.models import DynamicSetting

			for dynamic_setting in DynamicSetting.objects.all():
				DB_DYNAMIC_SETTINGS[dynamic_setting.key] = dynamic_setting.value

		except:
			pass

		return DB_DYNAMIC_SETTINGS

	@property
	def dynamic_settings(self):
		# Default useful info we'll add all the time
		DYNAMIC_SETTINGS = {
			"DEBUG": settings.DEBUG,
			"HOSTNAME": self.get_hostname(),
			"NOW": datetime.now(),
			# "CURRENT_DOMAIN": self.get_current_domain(request=request),
			"CACHE_STATS": self.get_cache_stats()
		}

		DYNAMIC_SETTINGS.update(self.get_dynamic_settings_from_settings())

		DYNAMIC_SETTINGS.update(self.get_dynamic_settings_from_db())

		return DYNAMIC_SETTINGS


class LazyDynamicSettings(LazyObject):
	def _setup(self):
		self._wrapped = DynamicSettings()

dynamic_settings = LazyDynamicSettings()
