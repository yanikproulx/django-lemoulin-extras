from django.contrib import admin

from lemoulin_extras.dynamic_settings import models


@admin.register(models.DynamicSetting)
class DynamicSettingAdmin(admin.ModelAdmin):
	list_display = ("key", "value", )
	search_fields = ["key", "value"]
