from django.apps import AppConfig


class DynamicSettingsConfig(AppConfig):
    name = 'lemoulin_extras.dynamic_settings'
