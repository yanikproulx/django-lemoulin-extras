# Gallery #

## Requirements ##
- Django 1.9*
- sorl

## Installation ##

in `settings.py`:

```
INSTALLED_APPS = (
    # ...
    'lemoulin_extras.gallery',
    'lemoulin_extras',
)
```

## Usage ##

in `models.py`:

```
from lemoulin_extras.gallery.models import Media

class ProductMedia(Media):
    product = models.ForeignKey(verbose_name=_("Product"), to=Product, related_name="media", )
```

*IMPORTANT* in `signals.py`:

```
from django.db.models.signals import pre_save
from django.dispatch import receiver

from . import models


@receiver(pre_save, sender=models.ProductMedia)
def set_Ppia_data(sender, instance, **kwargs):
    """Sets Product Media Data"""

    instance._set_media_data()
```

in `admin.py`:

```
from lemoulin_extras.gallery.admin import MediaAdminInline

class ProductMedia_Inline(MediaAdminInline):
    model = models.ProductMedia
```

in templates:

```
{% load extra_gallery_tags %}

{% show_gallery media=product.media.all %}
```
