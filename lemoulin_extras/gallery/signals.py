# Signals and receivers
from django.db.models.signals import pre_save
from django.dispatch import receiver

from lemoulin_extras.gallery import models


@receiver(pre_save, sender=models.Media)
def set_media_data(sender, instance, **kwargs):
	"""Sets Media Data"""

	instance._set_media_data()
