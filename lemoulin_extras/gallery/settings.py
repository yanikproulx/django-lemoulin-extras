from django.conf import settings

DEFAULT_GALLERY_MEDIA_TYPES = [
	"image",
	"image_url",
	"video_upload",
	"video_url",
	"file",
]

LE_MOULIN_EXTRAS_GALLERY_MEDIA_TYPES = getattr(settings, "LE_MOULIN_EXTRAS_GALLERY_MEDIA_TYPES", DEFAULT_GALLERY_MEDIA_TYPES)
