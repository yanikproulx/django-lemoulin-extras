# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-05-03 14:21-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lemoulin_extras/gallery/admin.py:40 lemoulin_extras/gallery/admin.py:136
#: lemoulin_extras/gallery/models.py:8 lemoulin_extras/gallery/models.py:36
msgid "Image"
msgstr ""

#: lemoulin_extras/gallery/admin.py:51 lemoulin_extras/gallery/admin.py:147
#: lemoulin_extras/gallery/models.py:38
msgid "Image URL"
msgstr ""

#: lemoulin_extras/gallery/admin.py:62 lemoulin_extras/gallery/admin.py:158
msgid "Uploaded Video"
msgstr ""

#: lemoulin_extras/gallery/admin.py:73 lemoulin_extras/gallery/admin.py:169
#: lemoulin_extras/gallery/models.py:49
msgid "Video URL"
msgstr ""

#: lemoulin_extras/gallery/admin.py:84 lemoulin_extras/gallery/admin.py:180
#: lemoulin_extras/gallery/models.py:10 lemoulin_extras/gallery/models.py:20
#: lemoulin_extras/gallery/models.py:57
msgid "File"
msgstr ""

#: lemoulin_extras/gallery/models.py:9 lemoulin_extras/gallery/models.py:46
msgid "Video"
msgstr ""

#: lemoulin_extras/gallery/models.py:14
msgid "YouTube"
msgstr ""

#: lemoulin_extras/gallery/models.py:15
msgid "Vimeo"
msgstr ""

#: lemoulin_extras/gallery/models.py:19 lemoulin_extras/gallery/models.py:38
msgid "Instagram"
msgstr ""

#: lemoulin_extras/gallery/models.py:30
msgid "Media Type"
msgstr ""

#: lemoulin_extras/gallery/models.py:31
msgid "Title"
msgstr ""

#: lemoulin_extras/gallery/models.py:32
msgid "Description"
msgstr ""

#: lemoulin_extras/gallery/models.py:34
msgid "Credit"
msgstr ""

#: lemoulin_extras/gallery/models.py:39
msgid "Image Source"
msgstr ""

#: lemoulin_extras/gallery/models.py:40
msgid "Image Embed HTML"
msgstr ""

#: lemoulin_extras/gallery/models.py:41
msgid "Image Thumbnail URL"
msgstr ""

#: lemoulin_extras/gallery/models.py:42
msgid "Image Author URL"
msgstr ""

#: lemoulin_extras/gallery/models.py:43
msgid "Image orginal filemane"
msgstr ""

#: lemoulin_extras/gallery/models.py:44
msgid "Image source base64"
msgstr ""

#: lemoulin_extras/gallery/models.py:47
msgid "Video Thumbnail"
msgstr ""

#: lemoulin_extras/gallery/models.py:49
msgid "Youtube or Vimeo URL"
msgstr ""

#: lemoulin_extras/gallery/models.py:50
msgid "Video Source"
msgstr ""

#: lemoulin_extras/gallery/models.py:51
msgid "Video Width"
msgstr ""

#: lemoulin_extras/gallery/models.py:52
msgid "Video Height"
msgstr ""

#: lemoulin_extras/gallery/models.py:53
msgid "Video Thumbnail URL"
msgstr ""

#: lemoulin_extras/gallery/models.py:54
msgid "Video ID"
msgstr ""

#: lemoulin_extras/gallery/models.py:55
msgid "Video Duration"
msgstr ""

#: lemoulin_extras/gallery/models.py:58
msgid "File Thumbnail"
msgstr ""

#: lemoulin_extras/gallery/models.py:60
msgid "Position"
msgstr ""

#: lemoulin_extras/gallery/models.py:62
msgid "Date taken"
msgstr ""

#: lemoulin_extras/gallery/models.py:75
msgid "Preview"
msgstr ""

#: lemoulin_extras/gallery/models.py:162 lemoulin_extras/gallery/models.py:163
msgid "Media"
msgstr ""
