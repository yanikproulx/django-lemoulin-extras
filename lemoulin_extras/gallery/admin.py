from django.contrib import admin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from lemoulin_extras.gallery import models
from lemoulin_extras.gallery import settings as gallery_settings

media_types = gallery_settings.LE_MOULIN_EXTRAS_GALLERY_MEDIA_TYPES


@admin.register(models.Media)
class MediaAdmin(admin.ModelAdmin):
	list_display = ("title", "media_type", "created", )
	ordering = ["title", "media_type", "created", ]
	search_fields = ["title", "media_type", "description", ]
	list_filter = ("media_type", "image_source", "video_source", )
	readonly_fields = (
		"image_source",
		"video_source",
		"video_width",
		"video_height",
		"video_thumbnail_url",
		"video_duration",
		"video_id",
		"media_type",
	)

	fieldsets = (
		(
			None,
			{
				'fields': ('title', 'description', 'credit', "position", )
			}
		),
	)

	if "image" in media_types:
		fieldsets += (
			(
				_("Image"),
				{
					'classes': ('collapse', ),
					'fields': ('image', )
				}
			),
		)

	if "image_url" in media_types:
		fieldsets += (
			(
				_("Image URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('image_url', "image_source", "image_embed_html", "image_thumbnail_url", "image_author_url", )
				}
			),
		)

	if "video_upload" in media_types:
		fieldsets += (
			(
				_("Uploaded Video"),
				{
					'classes': ('collapse', ),
					'fields': ('video', 'video_thumbnail', )
				}
			),
		)

	if "video_url" in media_types:
		fieldsets += (
			(
				_("Video URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('video_url', "video_source", "video_width", "video_height", "video_thumbnail_url", "video_duration", "video_id", )
				}
			),
		)

	if "file" in media_types:
		fieldsets += (
			(
				_("File"),
				{
					'classes': ('collapse', ),
					'fields': ('file', 'file_thumbnail', )
				}
			),
		)

	fieldsets += (
		(
			None,
			{
				'classes': ('collapse closed', ),
				'fields': ('media_type', )
			}
		),
	)

	class Media:
		if "GRAPPELLI_URL" in settings.INSTALLED_APPS:
			js = [
				settings.GRAPPELLI_URL + 'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL + 'tinymce_setup/tinymce_setup.js',
			]


class MediaAdminInline(admin.StackedInline):
	model = models.Media
	extra = 1
	readonly_fields = (
		"image_source",
		"video_source",
		"video_width",
		"video_height",
		"video_thumbnail_url",
		"video_duration",
		"video_id",
		"media_type",
	)

	fieldsets = (
		(
			None,
			{
				'fields': ('title', 'description', 'credit', "position", )
			}
		),
	)

	if "image" in media_types:
		fieldsets += (
			(
				_("Image"),
				{
					'classes': ('collapse', ),
					'fields': ('image', )
				}
			),
		)

	if "image_url" in media_types:
		fieldsets += (
			(
				_("Image URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('image_url', "image_source", "image_embed_html", "image_thumbnail_url", "image_author_url", )
				}
			),
		)

	if "video_upload" in media_types:
		fieldsets += (
			(
				_("Uploaded Video"),
				{
					'classes': ('collapse', ),
					'fields': ('video', 'video_thumbnail', )
				}
			),
		)

	if "video_url" in media_types:
		fieldsets += (
			(
				_("Video URL"),
				{
					'classes': ('collapse closed', ),
					'fields': ('video_url', "video_source", "video_width", "video_height", "video_thumbnail_url", "video_duration", "video_id", )
				}
			),
		)

	if "file" in media_types:
		fieldsets += (
			(
				_("File"),
				{
					'classes': ('collapse', ),
					'fields': ('file', 'file_thumbnail', )
				}
			),
		)

	fieldsets += (
		(
			None,
			{
				'classes': ('collapse closed', ),
				'fields': ('media_type', )
			}
		),
	)
	sortable_field_name = "position"

	class Media:
		if "GRAPPELLI_URL" in settings.INSTALLED_APPS:
			js = [
				settings.GRAPPELLI_URL + 'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL + 'tinymce_setup/tinymce_setup.js',
			]
