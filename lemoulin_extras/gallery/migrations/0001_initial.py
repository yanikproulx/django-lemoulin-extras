# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-29 17:19
from __future__ import unicode_literals

from django.db import migrations, models
import lemoulin_extras.gallery.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('media_type', models.CharField(blank=True, choices=[('image', 'Image'), ('video', 'Video'), ('file', 'File')], max_length=25, null=True, verbose_name='Media Type')),
                ('title', models.CharField(blank=True, max_length=1024, null=True, verbose_name='Title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description')),
                ('credit', models.CharField(blank=True, max_length=200, null=True, verbose_name='Credit')),
                ('image', models.ImageField(blank=True, null=True, upload_to=lemoulin_extras.gallery.models.get_media_file_path, verbose_name='Image')),
                ('image_url', models.URLField(blank=True, help_text='Instagram', null=True, verbose_name='Image URL')),
                ('image_source', models.CharField(blank=True, choices=[('instagram', 'Instagram'), ('file', 'File')], max_length=25, null=True, verbose_name='Image Source')),
                ('image_embed_html', models.TextField(blank=True, null=True, verbose_name='Image Embed HTML')),
                ('image_thumbnail_url', models.URLField(blank=True, null=True, verbose_name='Image Thumbnail URL')),
                ('image_author_url', models.URLField(blank=True, null=True, verbose_name='Image Author URL')),
                ('image_filename', models.CharField(blank=True, max_length=255, null=True, verbose_name='Image orginal filemane')),
                ('image_base64', models.TextField(blank=True, null=True, verbose_name='Image source base64')),
                ('video', models.FileField(blank=True, null=True, upload_to=lemoulin_extras.gallery.models.get_media_file_path, verbose_name='Video')),
                ('video_thumbnail', models.ImageField(blank=True, null=True, upload_to=lemoulin_extras.gallery.models.get_media_file_path, verbose_name='Video Thumbnail')),
                ('video_url', models.URLField(blank=True, help_text='Youtube or Vimeo URL', null=True, verbose_name='Video URL')),
                ('video_source', models.CharField(blank=True, choices=[('youtube', 'YouTube'), ('vimeo', 'Vimeo')], max_length=25, null=True, verbose_name='Video Source')),
                ('video_width', models.PositiveIntegerField(blank=True, null=True, verbose_name='Video Width')),
                ('video_height', models.PositiveIntegerField(blank=True, null=True, verbose_name='Video Height')),
                ('video_thumbnail_url', models.URLField(blank=True, null=True, verbose_name='Video Thumbnail URL')),
                ('video_id', models.CharField(blank=True, max_length=25, null=True, verbose_name='Video ID')),
                ('video_duration', models.PositiveIntegerField(blank=True, null=True, verbose_name='Video Duration')),
                ('file', models.FileField(blank=True, null=True, upload_to=lemoulin_extras.gallery.models.get_media_file_path, verbose_name='File')),
                ('file_thumbnail', models.ImageField(blank=True, null=True, upload_to=lemoulin_extras.gallery.models.get_media_file_path, verbose_name='File Thumbnail')),
                ('position', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Position')),
                ('date_taken', models.DateTimeField(blank=True, null=True, verbose_name='Date taken')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Media',
                'verbose_name_plural': 'Media',
                'ordering': ['title'],
            },
        ),
    ]
