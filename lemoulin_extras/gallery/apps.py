from django.apps import AppConfig


class GalleryConfig(AppConfig):
	name = 'lemoulin_extras.gallery'

	def ready(self):
		import lemoulin_extras.gallery.signals
