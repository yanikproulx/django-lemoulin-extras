# Cache Manager #

## Installation ##

in `settings.py`:

```
INSTALLED_APPS = (
	# ...
	'lemoulin_extras.cache',
    'lemoulin_extras',
)
```

in `urls.py`:

```
url(r'^lemoulin_extras/', include("lemoulin_extras.urls", namespace="lemoulin_extras")),
```

or

```
url(r'^lemoulin_extras/cache/', include("lemoulin_extras.cache.urls", namespace="lemoulin_extras_cache")),
```

## Usage ##

view:

'http://[yourdomain]/lemoulin_extras/cache/clear/'

command line:

'manage.py clearcache'
