from django.conf.urls import url

from .views import ClearCacheView

urlpatterns = [
	url(r'^clear/$', ClearCacheView.as_view(), name="clear"),
]
