from django.utils.translation import ugettext_lazy as _
from django.core.management.base import BaseCommand


class Command(BaseCommand):
	help = "Clears the cache"

	def handle(self, *args, **options):
		from django.core.cache import cache
		cache.clear()

		self.stdout.write(_("Cache cleared"))
