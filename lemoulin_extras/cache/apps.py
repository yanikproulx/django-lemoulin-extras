from django.apps import AppConfig


class CacheConfig(AppConfig):
    name = 'lemoulin_extras.cache'
