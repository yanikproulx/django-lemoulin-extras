from django.conf import settings

DEFAULT_PAGE_MEDIA_TYPES = [
	"image",
	"video_url",
]

LE_MOULIN_EXTRAS_PAGE_MEDIA_TYPES = getattr(settings, "LE_MOULIN_EXTRAS_PAGE_MEDIA_TYPES", DEFAULT_PAGE_MEDIA_TYPES)
