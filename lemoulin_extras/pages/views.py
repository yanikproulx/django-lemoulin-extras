from django.views.generic import DetailView, TemplateView
from meta.views import MetadataMixin

from lemoulin_extras.l10n.views import LanguageMixin
from lemoulin_extras.pages import models


class StaticPageView(LanguageMixin, MetadataMixin, DetailView):
	model = models.Page
	context_object_name = "page"
	template_name = "lemoulin_extras/pages/default.html"

	def get_context_data(self, **kwargs):
		context = super(StaticPageView, self).get_context_data(**kwargs)
		context['meta'] = self.get_object().as_meta(self.request)
		return context

	def dispatch(self, request, *args, **kwargs):
		self.template = kwargs.get("template", None)

		return super(StaticPageView, self).dispatch(request, *args, **kwargs)

	def get_template_names(self):
		obj = self.get_object()

		if self.template:
			return self.template
		elif getattr(obj, "template_name", None):
			return getattr(obj, "template_name")
		else:
			return super(StaticPageView, self).get_template_names()


class TextPlainView(TemplateView):
	def render_to_response(self, context, **kwargs):
		return super(TextPlainView, self).render_to_response(context, content_type='text/plain', **kwargs)
