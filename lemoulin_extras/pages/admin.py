from django.contrib import admin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from lemoulin_extras.gallery.admin import MediaAdminInline
from lemoulin_extras.pages import models
from lemoulin_extras.pages import settings as page_settings

media_types = page_settings.LE_MOULIN_EXTRAS_PAGE_MEDIA_TYPES


class PageMediaInline(MediaAdminInline):
	model = models.PageMedia
	extra = 0
	readonly_fields = ('admim_thumbnail_preview',)
	fieldsets = (
		(
			None,
			{
				'fields': ('title', 'description', 'credit', "position",)
			}
		),
	)

	if "image" in media_types:
		fieldsets = fieldsets + (
			(
				_("Image"),
				{
					'classes': ('collapse',),
					'fields': ('image', 'admim_thumbnail_preview', 'image_base64', )
				}
			),
		)

	if "image_url" in media_types:
		fieldsets = fieldsets + (
			(
				_("Image URL"),
				{
					'classes': ('collapse closed',),
					'fields': ('image_url', "image_source", "image_embed_html", "image_thumbnail_url", "image_author_url", )
				}
			),
		)

	if "video_url" in media_types:
		fieldsets = fieldsets + (
			(
				_("Video URL"),
				{
					'classes': ('collapse',),
					'fields': ('video_url', "video_source", "video_width", "video_height", "video_thumbnail_url", "video_duration", "video_id",)
				}
			),
		)

	if "video_upload" in media_types:
		fieldsets = fieldsets + (
			(
				_("Uploaded Video"),
				{
					'classes': ('collapse',),
					'fields': ('video', 'video_thumbnail', )
				}
			),
		)

	if "file" in media_types:
		fieldsets = fieldsets + (
			(
				_("File"),
				{
					'classes': ('collapse',),
					'fields': ('file', 'file_thumbnail',)
				}
			),
		)


class PageSubSection_Inline(admin.StackedInline):
	model = models.PageSubSection
	extra = 1
	sortable_field_name = "order"
	can_delete = True

	class Media:
		if "GRAPPELLI_URL" in settings.INSTALLED_APPS:
			js = [
				settings.GRAPPELLI_URL + 'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL + 'tinymce_setup/tinymce_setup.js',
			]


@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
	list_display = ("title", "slug", "language_code", )
	ordering = ["title", "language_code", ]
	prepopulated_fields = {"slug": ("title", ), }
	search_fields = ["title", "language_code", "text", ]
	list_filter = ("language_code", )
	inlines = [PageSubSection_Inline, PageMediaInline, ]
	change_form_template = "lemoulin_extras/pages/admin/change_form.html"

	class Media:
		if settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL + 'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL + 'tinymce_setup/tinymce_setup.js',
			]


@admin.register(models.StaticText)
class StaticTextAdmin(admin.ModelAdmin):
	list_display = ("slug", "title", "language_code", )
	ordering = ["slug", "language_code", ]
	prepopulated_fields = {"slug": ("title", ), }
	list_filter = ("language_code", )
	save_as = True

	class Media:
		if settings.GRAPPELLI_URL:
			js = [
				settings.GRAPPELLI_URL + 'tinymce/jscripts/tiny_mce/tiny_mce.js',
				settings.GRAPPELLI_URL + 'tinymce_setup/tinymce_setup.js',
			]
