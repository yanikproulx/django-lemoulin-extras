from django.db import models
from django.utils.translation import ugettext_lazy as _

from sorl.thumbnail import get_thumbnail
from meta.models import ModelMeta

from lemoulin_extras.l10n.models import MultilingualModel
from lemoulin_extras.gallery.models import Media
from lemoulin_extras.dynamic_settings import dynamic_settings
from lemoulin_extras.utils.strings import strip_html


def get_pages_image_path(instance, filename):
	from lemoulin_extras.utils.files import clean_filename

	return "pages/{filename}".format(filename=clean_filename(filename))


class Page(ModelMeta, MultilingualModel):
	title = models.CharField(verbose_name=_('title'), max_length=200, )
	slug = models.SlugField(verbose_name=_("Label"), max_length=50, unique=False, )
	header_title = models.CharField(verbose_name=_("Header main title"), max_length=1024, blank=True, null=True, )
	sub_title = models.CharField(verbose_name=_("Sub Title"), max_length=1024, blank=True, null=True, )
	image = models.ImageField(verbose_name=_("Image"), upload_to=get_pages_image_path, null=True, blank=True, )
	text = models.TextField(verbose_name=_('Content'), blank=True, )
	template_name = models.CharField(verbose_name=_('template name'), max_length=70, blank=True, null=True, help_text=_("Example: 'static/default.html'"), )

	_metadata = {
		'title': 'seo_title',
		'description': 'seo_description',
		'image': 'seo_image',
	}

	def __str__(self):
		return self.title

	@property
	def seo_title(self):
		try:
			return "{title} — {site_name}".format(title=self.title, site_name=dynamic_settings.get("SITE_NAME", None))
		except:
			return self.title

	@property
	def seo_description(self):
		return strip_html(self.text)

	@property
	def seo_image(self):
		try:
			image = get_thumbnail(self.image, '900')
			return image.url
		except:
			return dynamic_settings.get("DEFAULT_SHARE_IMAGE_PATH", None)

	class Meta:
		verbose_name = _('Page')
		verbose_name_plural = _('Pages')
		ordering = ("title",)
		unique_together = (("language_code", "slug", ),)


class PageMedia(Media):
	page = models.ForeignKey(to=Page, related_name="media")

	class Meta:
		ordering = ["position", "-created", ]
		verbose_name = _('Page Media')
		verbose_name_plural = _('Page Media')

	def __str__(self):
		return "{page_title}: {title} ({media_type})".format(page_title=self.page.title, title=self.title, media_type=self.media_type)


class PageSubSection(models.Model):
	page = models.ForeignKey(to=Page, related_name="subsections", )
	title = models.CharField(verbose_name=_("Title"), max_length=200, null=True, blank=True, )
	text = models.TextField(verbose_name=_('Content'), blank=True, )
	image = models.ImageField(verbose_name=_("Image"), upload_to=get_pages_image_path, null=True, blank=True, )
	credit = models.CharField(verbose_name=_("Image Credit"), max_length=200, null=True, blank=True, )
	order = models.PositiveSmallIntegerField(verbose_name=_("Display order"), null=True, blank=True, )

	def __str__(self):
		if self.title:
			return "{page_title}: {title} ({order})".format(page_title=self.page.title, title=self.title, order=self.order)
		else:
			return "{page_title} ({order})".format(page_title=self.page.title, order=self.order)

	class Meta:
		ordering = ["order"]
		verbose_name = _("Page Sub Section")
		verbose_name_plural = _("Pages Sub Sections")


class StaticText(MultilingualModel):
	title = models.CharField(verbose_name=_('Title'), max_length=200, blank=True, null=True, )
	slug = models.SlugField(verbose_name=_("Label"), max_length=50, unique=False, )
	subtitle = models.CharField(verbose_name=_('Sub-Title'), max_length=200, blank=True, null=True, )
	text = models.TextField(verbose_name=_("Text"), blank=True, null=True, )

	def __str__(self):
		return "{slug} ({language_code})".format(slug=self.slug, language_code=self.language_code)

	class Meta:
		unique_together = ("language_code", "slug")
		ordering = ["slug"]
		verbose_name = _("Static Text")
		verbose_name_plural = _("Static Texts")
		unique_together = (("language_code", "slug", ),)
