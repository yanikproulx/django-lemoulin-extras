from django.apps import AppConfig


class PagesConfig(AppConfig):
	name = 'lemoulin_extras.pages'

	def ready(self):
		import lemoulin_extras.pages.signals
