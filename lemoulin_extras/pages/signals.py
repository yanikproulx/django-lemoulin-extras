# Signals and receivers
from django.db.models.signals import pre_save
from django.dispatch import receiver

from lemoulin_extras.pages import models


@receiver(pre_save, sender=models.PageMedia)
def set_page_media_data(sender, instance, **kwargs):
	"""Sets Page Media Data"""

	instance._set_media_data()
	instance._save_base64_data()
