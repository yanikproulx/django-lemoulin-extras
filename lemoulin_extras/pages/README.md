# Pages #

## Requirements ##
- Django 1.9*
- Sites framework (because django-meta)
- django-meta
- lemoulin_extras.utils
- lemoulin_extras.gallery

## Installation ##

in `settings.py`:

```
INSTALLED_APPS = (
    # ...
    'meta',
    'lemoulin_extras.gallery',
    'lemoulin_extras.utils',
    'lemoulin_extras.pages',
    'lemoulin_extras',
)


# Meta
META_USE_SITES = True
META_USE_TITLE_TAG = True
META_USE_TWITTER_PROPERTIES = True
META_USE_OG_PROPERTIES = True
META_SITE_PROTOCOL = "http"
```

## Usage ##

in `urls.py`:

```
urlpatterns = [
    # ...
    'meta',
    url(r'^about/$', StaticPageView.as_view(), {"slug": "about", }, name="about"),
]
```
