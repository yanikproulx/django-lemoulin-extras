from django.apps import AppConfig


class ExtrasConfig(AppConfig):
    name = 'lemoulin_extras'
