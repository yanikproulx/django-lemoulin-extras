from setuptools import setup, find_packages

version = __import__('lemoulin_extras').__version__

packages = find_packages()

setup(
	name='django-lemoulin-extras',
	packages=packages,
	version=version,
	description='Tools and utilities for Django.',
	author='Yanik Proulx',
	author_email='yanikproulx@lemoulin.co',
	url='https://bitbucket.org/lemoulin/django-lemoulin-extras',
	download_url='https://bitbucket.org/lemoulin/django-lemoulin-extras',
	classifiers=[
		'Environment :: Web Environment',
		'Framework :: Django',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: MIT License',
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 3',
	],
	scripts=[],
	license='LICENSE.txt',
	long_description=open('README.md').read(),
	tests_require=[
		"Django>=1.9,<1.10",
	],
	install_requires=[
		'requests',
		'sorl-thumbnail',
		# 'django-meta',
	],
	dependency_links=[
	],
	include_package_data=True,
)
